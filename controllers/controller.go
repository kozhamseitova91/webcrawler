package controller

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"github.com/go-gorp/gorp"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"strconv"
	"webCrawler/models"
)

type Controller struct {
	dbMap *gorp.DbMap
}

func NewController(db *sql.DB) *Controller {
	dbMap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{Engine: "InnoDB", Encoding: "UTF8"}}
	err := dbMap.CreateTablesIfNotExists()
	checkErr(err, "Create tables failed")
	return &Controller{dbMap: dbMap}
}
func checkErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}
func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func (c *Controller) GetNews(ctx *gin.Context) {
	var news []models.News
	_, err := c.dbMap.Select(&news, "select * from news")
	if err == nil {
		ctx.JSON(200, news)
	} else {
		ctx.JSON(404, gin.H{"error": "news not found"})
	}
}

func (c *Controller) CreateNews(ctx *gin.Context) {
	var news models.News
	if err := ctx.ShouldBindJSON(&news); err != nil {
		ctx.JSON(400, gin.H{"error": "invalid request"})
		return
	}

	// Insert news into database
	_, err := c.dbMap.Exec("INSERT INTO news (title, description, link, img) VALUES (?, ?, ?, ?)",
		news.Title, news.Description, news.Link, news.Img)
	if err != nil {
		ctx.JSON(500, gin.H{"error": "failed to create news"})
		return
	}

	// Return success response
	ctx.JSON(200, gin.H{"message": "news created"})
}

func (c *Controller) DeleteNews(ctx *gin.Context) {
	// Get news ID from request parameters
	newsID := ctx.Param("id")

	// Delete news from database
	_, err := c.dbMap.Exec("DELETE FROM news WHERE id=?", newsID)
	if err != nil {
		ctx.JSON(500, gin.H{"error": "failed to delete news"})
		return
	}

	// Return success response
	ctx.JSON(200, gin.H{"message": "news deleted"})
}

func (c *Controller) GetNewsByPage(ctx *gin.Context) {
	param := ctx.Params.ByName("id")
	page, err := strconv.Atoi(param)
	if err != nil {
		page = 0
	}
	type PageInfo struct {
		TotalPages  int `json:"total_pages"`
		CurrentPage int `json:"current_page"`
	}
	pageInfo := PageInfo{}
	pageInfo.CurrentPage = page
	err = c.dbMap.Db.QueryRow("select FLOOR(count(*)/15) as total_pages from news;").Scan(&pageInfo.TotalPages)
	if err != nil {
		log.Println(err)
	}
	var news []models.News
	_, err = c.dbMap.Select(&news, "select * from news order by id desc limit 15 offset ?;", page*15)
	if err == nil {
		ctx.JSON(200, struct {
			News     []models.News `json:"news"`
			PageInfo PageInfo      `json:"page_info"`
		}{
			News:     news,
			PageInfo: pageInfo,
		})
	} else {
		ctx.JSON(404, gin.H{"error": "news not found"})
	}
}

func (c *Controller) GetNewsByKeywords(ctx *gin.Context) {
	keyword := ctx.Params.ByName("keyword")
	var news []models.News
	_, err := c.dbMap.Select(&news, "SELECT * FROM news WHERE title like ? or description like ?", "%"+keyword+"%", "%"+keyword+"%")
	if err == nil {
		ctx.JSON(200, news)
	} else {
		ctx.JSON(404, gin.H{"error": "news not found"})
	}
}
