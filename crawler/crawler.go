package crawler

import (
	"database/sql"
	"github.com/go-co-op/gocron"
	"time"
)

func RunCronJobs(db *sql.DB) {
	// go crawlerInformAllPages(db)
	// 3
	s := gocron.NewScheduler(time.UTC)
	// 4
	s.Every(1000).Seconds().Do(func() {
		crawlerInform(db)
	})

	// 5
	s.StartAsync()
}
