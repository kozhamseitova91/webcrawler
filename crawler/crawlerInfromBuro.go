package crawler

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	_ "github.com/go-sql-driver/mysql"
)

func crawlerIB() {

	db, err := sql.Open("mysql", "root:1234@tcp(localhost:3306)/webCrawler")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Make an HTTP GET request to the URL
	res, err := http.Get("https://informburo.kz/novosti")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	// Check the status code of the response
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Select the news items from the HTML document
	var news []News
	doc.Find(".uk-width-expand").Each(func(i int, s *goquery.Selection) {
		title := s.Find("a").Text()
		description := ""
		link, _ := s.Find("a").Attr("href")
		news = append(news, News{title, description, link, "img"})
	})

	// Print the news information
	for _, item := range news {
		stmt, err := db.Prepare("INSERT INTO news (title, description, link) VALUES (?, ?, ?)")
		if err != nil {
			log.Fatal(err)
		}
		res, err := stmt.Exec(item.Title, item.Description, item.Link)
		if err != nil {
			log.Fatal(err)
		}
		lastID, err := res.LastInsertId()
		if err != nil {
			log.Fatal(err)
		}
		rowCnt, err := res.RowsAffected()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("ID = %d, affected = %d\n", lastID, rowCnt)
	}
}
