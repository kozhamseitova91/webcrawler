
.PHONY: migrate-up
migrate-up:
	migrate create -ext sql -dir db/migrations -seq ${name}
