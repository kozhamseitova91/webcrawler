CREATE TABLE `webCrawler`.`news`
(
    `id`          INT          NOT NULL AUTO_INCREMENT,
    `title`       VARCHAR(510) NOT NULL,
    `description` TEXT         NOT NULL,
    `link`        TEXT         NOT NULL,
    `img`         TEXT         NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
    UNIQUE INDEX `title_UNIQUE` (`title` ASC) VISIBLE
);