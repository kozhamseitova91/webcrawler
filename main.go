package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mysql"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"webCrawler/crawler"

	"log"
	"webCrawler/config"
	mapping "webCrawler/mappings"
)

func main() {
	config, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(config)
	db, err := sql.Open("mysql", config.MySqlSource)
	/*db, err := sql.Open("mysql", "crawler:Aisha123@@tcp(crawler-mysql.mysql.database.azure.com)/webCrawler")
	if err != nil {
		log.Fatal(err)
	}*/
	defer db.Close()
	driver, err := mysql.WithInstance(db, &mysql.Config{})
	if err != nil {
		log.Println(err)
	}
	m, err := migrate.NewWithDatabaseInstance("file://db/migrations", "webCrawler", driver)
	if err != nil {
		log.Printf("Err migrating: %v", err)
	}

	err = m.Up()
	if err != nil {
		log.Println("err migrating: %v", err)
	}

	crawler.RunCronJobs(db)

	mapping.CreateUrlMappings(db)
	mapping.Router.Run(":" + config.Port)
}
